package ru.t1.stepanishchev.tm.command.task;

import ru.t1.stepanishchev.tm.enumerated.ProjectSort;
import ru.t1.stepanishchev.tm.enumerated.TaskSort;
import ru.t1.stepanishchev.tm.model.Task;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    private final String NAME = "task-list";

    private final String DESCRIPTION = "Show list tasks.";

    @Override
    public void execute() {
            System.out.println("[TASK LIST]");
            System.out.println("ENTER SORT:");
            System.out.println(Arrays.toString(TaskSort.values()));
            final String sortType = TerminalUtil.nextLine();
            final TaskSort sort = TaskSort.toSort(sortType);
            final String userId = getUserId();
            final List<Task> tasks = getTaskService().findAll(userId, sort.getComparator());
            int index = 1;
            for (final Task task : tasks) {
                if (task == null) continue;
                System.out.println(index + ". " + task.getName() + " : " + task.getDescription() + " : " + task.getId());
                index++;
            }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}