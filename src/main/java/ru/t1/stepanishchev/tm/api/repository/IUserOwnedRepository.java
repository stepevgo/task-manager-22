package ru.t1.stepanishchev.tm.api.repository;

import ru.t1.stepanishchev.tm.enumerated.ProjectSort;
import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    M add(String userId, M model);

    List<M> findAll(final String userId);

    List<M> findAll(final String userId, final Comparator<M> comparator);

    List<M> findAll(final String userId, final ProjectSort sort);

    M findOneByIndex(final String userId, final Integer index);

    M findOneById(final String userId, final String id);

    boolean existsById(final String userId, final String id);

    M removeOne (final String userId, final M model);

    M removeOneByIndex(final String userId, final Integer index);

    M removeOneById(final String userId, final String id);

    void removeAll(final String userId);

    int getSize(final String userId);

}