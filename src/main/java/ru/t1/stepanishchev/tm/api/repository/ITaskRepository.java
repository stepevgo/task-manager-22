package ru.t1.stepanishchev.tm.api.repository;

import ru.t1.stepanishchev.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    Task create(String userId, String name, String description);

    Task create(String userId, String name);

    List<Task> findAllByProjectId(String userId, String projectId);

}