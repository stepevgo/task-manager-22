package ru.t1.stepanishchev.tm.enumerated;

import ru.t1.stepanishchev.tm.comparator.CreatedComparator;
import ru.t1.stepanishchev.tm.comparator.NameComparator;
import ru.t1.stepanishchev.tm.comparator.StatusComparator;
import ru.t1.stepanishchev.tm.model.Project;

import java.util.Comparator;

public enum ProjectSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_DEFAULT("Without sort", null);

    private final String name;

    private final Comparator<Project> comparator;

    public static ProjectSort toSort(final String value) {
        if (value == null || value.isEmpty()) return BY_DEFAULT;
        for (final ProjectSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return BY_DEFAULT;
    }

    ProjectSort(final String displayName, final Comparator<Project> comparator) {
        this.name = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return name;
    }

    public Comparator<Project> getComparator() {
        return comparator;
    }

}