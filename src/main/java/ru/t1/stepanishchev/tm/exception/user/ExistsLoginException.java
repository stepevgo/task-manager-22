package ru.t1.stepanishchev.tm.exception.user;

public final class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! Login alredy exists.");
    }

    public ExistsLoginException(String login) {
        super("Error! Login '" + login + "' alredy exists.");
    }

}