package ru.t1.stepanishchev.tm.repository;

import ru.t1.stepanishchev.tm.api.repository.IRepository;
import ru.t1.stepanishchev.tm.enumerated.ProjectSort;
import ru.t1.stepanishchev.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(final ProjectSort sort) {
        if (sort == null) return findAll();
        return findAll(sort);
    }


    @Override
    public M findOneById(final String id) {
        return models
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public M removeOne(final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M removeOneById(final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public M removeOneByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public void removeAll() {
        models.clear();
    }

    @Override
    public boolean existsById(String id) {
        return findOneById(id) != null;
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        models.removeAll(collection);
    }

    @Override
    public Integer getSize() {
        return models.size();
    }

}