package ru.t1.stepanishchev.tm.repository;

import ru.t1.stepanishchev.tm.api.repository.IUserOwnedRepository;
import ru.t1.stepanishchev.tm.enumerated.ProjectSort;
import ru.t1.stepanishchev.tm.exception.entity.ModelNotFoundException;
import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public List<M> findAll(final String userId) {
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        final List<M> models = findAll(userId);
        models.sort(comparator);
        return models;
    }

    @Override
    public List<M> findAll(final String userId, final ProjectSort sort) {
        return super.findAll(sort);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneById(final String userId, final String id) {
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        findOneById(userId, id);
        return true;
    }

    @Override
    public M removeOne(final String userId, final M model) {
        if (userId.equals(model.getUserId())) {
            models.remove(model);
        }
        return model;
    }

    @Override
    public M removeOneByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) throw new ModelNotFoundException();
        return removeOne(userId, model);
    }

    @Override
    public M removeOneById(final String userId, final String id) {
        final M model = findOneById(userId, id);
        return removeOne(userId, model);
    }

    @Override
    public void removeAll(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public int getSize(final String userId) {
        return (int) models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

}